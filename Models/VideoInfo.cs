﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElectronNetPlayerJpys.Models
{
    /// <summary>
    /// 视频基本信息类
    /// </summary>
    public class VideoInfo
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 基本信息链接
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 更新时间（情况）
        /// </summary>
        public string Date { get; set; }
    }
}
