﻿using AngleSharp;
using AngleSharp.Dom;


namespace ElectronNetPlayerJpys.Common
{
    /// <summary>
    /// AngleSharp解析HTML
    /// </summary>
    public static class ParseHtml
    {
        /// <summary>
        /// AngleSharp解析html字符串为IDocument对象
        /// </summary>
        /// <param name="htmlCode">html字符串</param>
        /// <returns>IDocument对象</returns>
        public static IDocument GetDocument(string htmlCode)
        {
            var config = Configuration.Default;
            var context = BrowsingContext.New(config);
            return context.OpenAsync(req => req.Content(htmlCode)).Result;
        }
    }
}
