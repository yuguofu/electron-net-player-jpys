﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElectronNetPlayerJpys.Models
{
    /// <summary>
    /// 视频详情类
    /// </summary>
    public class VideoDetailInfo
    {
        /// <summary>
        /// 视频名称
        /// </summary>
        public string Title { get; set; } = "";
        /// <summary>
        /// 简介
        /// </summary>
        public string Introduction { get; set; } = "";

        /// <summary>
        /// 播放信息列表
        /// </summary>
        public List<PlayInfo> PlayInfo { get; set; } = new List<PlayInfo>();
    }
}
