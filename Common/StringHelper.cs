﻿namespace ElectronNetPlayerJpys.Common
{
    public static class StringHelper
    {
        /// <summary>
        /// 获取中间文本
        /// </summary>
        /// <param name="left">左边字符串</param>
        /// <param name="right">右边字符串</param>
        /// <param name="text">待处理字符串</param>
        /// <returns></returns>
        public static string GetTextGainCenter(string left, string right, string text)
        {
            //判断是否为null或者是empty
            if (string.IsNullOrEmpty(left))
                return "";
            if (string.IsNullOrEmpty(right))
                return "";
            if (string.IsNullOrEmpty(text))
                return "";
            //判断是否为null或者是empty

            int Lindex = text.IndexOf(left); //搜索left的位置

            if (Lindex == -1)
            { //判断是否找到left
                return "";
            }

            Lindex = Lindex + left.Length; //取出left右边文本起始位置

            int Rindex = text.IndexOf(right, Lindex);//从left的右边开始寻找right

            if (Rindex == -1)
            {//判断是否找到right
                return "";
            }

            return text.Substring(Lindex, Rindex - Lindex);//返回查找到的文本
        }
    }
}
