﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElectronNetPlayerJpys.Models
{
    /// <summary>
    /// 视频播放信息类
    /// </summary>
    public class PlayInfo
    {
        /// <summary>
        /// 集数
        /// </summary>
        public string Episode { get; set; } = "";
        /// <summary>
        /// 播放链接（非真实地址）
        /// </summary>
        public string Address { get; set; } = "";
    }
}
