# ElectronNetPlayerJpys

#### 介绍
基于Electron.Net的跨平台网络视频采集播放软件

#### 软件架构
Electron.Net(.net core mvc、.net5) + elementUI + video.js  


#### 安装教程

1.  安装ElectronNET.CLI  

``` 
    dotnet tool install ElectronNET.CLI -g
```

2.  初始化  
``` 
    electronize init
```  

3.  运行  
``` 
    electronize start
```  
  
4.  打包  
    
```
    electronize build /target win
    electronize build /target osx
    electronize build /target linux
```


#### 说明

Electron.NET的GitHub地址：  https://github.com/ElectronNET/Electron.NET  



#### 预览

![1](https://images.gitee.com/uploads/images/2021/0928/213855_b840d270_5348320.png "屏幕截图.png")
![2](https://images.gitee.com/uploads/images/2021/0928/213927_f5a6ac86_5348320.png "屏幕截图.png")
![3](https://images.gitee.com/uploads/images/2021/0928/214022_8d936085_5348320.png "屏幕截图.png")